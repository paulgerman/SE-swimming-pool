package com.company;

public class JeanLuc extends Thread {
	private SwimmingPool swimmingPool;

	public JeanLuc(SwimmingPool swimmingPool) {
		this.swimmingPool = swimmingPool;
	}

	@Override
	public synchronized void run() {
		super.run();
		//will clean forever
		while (true) {

			//wait random interval before cleaning
			try {
				sleep((long) (5000 + Math.random() * 10));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Jean wants to clean!");

			//prepare to clean (stop clients from getting in the pool)
			swimmingPool.getPool().prepareCleaning();
			System.out.println("Jean started cleaning (" + swimmingPool.getPool().getClientsNum() + " clients in the pool)!");


			//clean!
			try {
				sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Jean finished cleaning!");

			//finish cleaning (allow clients in the pool)
			swimmingPool.getPool().finishCleaning();
		}
	}
}
