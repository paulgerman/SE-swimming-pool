package com.company;

import static java.lang.Thread.sleep;

public class ReceptionCounter extends SwimmingPoolPlace {

	public ReceptionCounter(SwimmingPool swimmingPool, int nbPlaces, String name) {
		super(swimmingPool, nbPlaces, name);
	}

	@Override
	public synchronized void doAction(Client client) {
		System.out.println("Client " + client.getClientName() + " started getting ticket in counter " + this.getPlaceName());

		//sell ticket to client
		try {
			sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.removeClient(client);
		System.out.println("Client " + client.getClientName() + " finished getting ticket in counter " + this.getPlaceName());

		//move client to the changing room
		swimmingPool.getChangingRoom().addClient(client);
	}
}
