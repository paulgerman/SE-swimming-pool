package com.company;

import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) {
		SwimmingPool swimmingPool = new SwimmingPool(2,10,10,4,9999,100);

		int nbClients = 10;


		for(int i = 0; i < nbClients; i++)
		{
			Client client = new Client(swimmingPool, "client"+i);
			client.start();
		}
	    try {
		    sleep(3000);
	    } catch (InterruptedException e) {
		    e.printStackTrace();
	    }
	    for(int i = 0; i < nbClients; i++)
	    {
		    Client client = new Client(swimmingPool, "client"+(nbClients + i));
		    client.start();
	    }
    }
}
