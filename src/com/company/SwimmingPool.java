package com.company;

import java.util.ArrayList;

public class SwimmingPool {

	private ArrayList<ReceptionCounter> receptionCounters;
	private ChangingRoom changingRoom;
	private SwimfinStore swimfinStore;
	private Pool pool;

	public SwimmingPool(int nbReceptionCounters, int receptionCounterCapacity, int changingRoomCapacity, int poolCapacity, int swimFinStoreCapacity, int nbSwimFins) {
		receptionCounters = new ArrayList<>();
		for (int i = 0; i < nbReceptionCounters; i++)
			receptionCounters.add(new ReceptionCounter(this, receptionCounterCapacity, "reception" + i));

		changingRoom = new ChangingRoom(this, changingRoomCapacity, "changing room");
		swimfinStore = new SwimfinStore(this, swimFinStoreCapacity, nbSwimFins, "swimfinstore");
		pool = new Pool(this, poolCapacity, "pool");
		JeanLuc jeanLuc = new JeanLuc(this);

		jeanLuc.start();
	}

	//welcome client to the swimming pool
	public synchronized void addClient(Client client) {
		ReceptionCounter receptionCounterToAddTo = null;
		int minClientsTotal = Integer.MAX_VALUE;

		//choose the best counter to direct the client to
		//we choose the counter with the least (clients buying right now + waiting clients)
		for (ReceptionCounter receptionCounter : receptionCounters) {
			int clientsTotal = receptionCounter.getClientsNum() + receptionCounter.getClientsWaitingNum();
			if (clientsTotal < minClientsTotal) {
				minClientsTotal = clientsTotal;
				receptionCounterToAddTo = receptionCounter;
			}
		}

		//add client to the chosen counter
		if (receptionCounterToAddTo != null)
			receptionCounterToAddTo.addClient(client);
	}

	public ChangingRoom getChangingRoom() {
		return changingRoom;
	}

	public SwimfinStore getSwimfinStore() {
		return swimfinStore;
	}

	public Pool getPool() {
		return pool;
	}
}
