/**
 * Created by martinpotel on 16/11/2017.
 */
package com.company;

import static java.lang.Thread.sleep;

public class Pool extends SwimmingPoolPlace {
	private int nbPlacesInitial;
    public Pool(SwimmingPool swimmingPool, int nbPlaces, String name) {
        super(swimmingPool, nbPlaces,name);
        nbPlacesInitial = nbPlaces;
    }


    //make the client swim
	@Override
	public void doAction(Client client) {
		System.out.println("Client "+client.getClientName()+" started swimming");

		try {
			sleep((long)(4000 + Math.random() * 10));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.removeClient(client);
		System.out.println("Client "+client.getClientName()+" finished swimming");

		if(client.hasSwimfins())
			swimmingPool.getSwimfinStore().addClient(client);
		else
			swimmingPool.getChangingRoom().addClient(client);
	}

	//remove client from the pool and notify Jean if he can clean
	@Override
	public synchronized void removeClient(Client client) {
		super.removeClient(client);

		if(getClientsNum() == 0 && getCapacity() == 0)
			notify();
	}

	//Stop clients from entering the pool to allow Jean to clean
	public synchronized void prepareCleaning(){
    	this.setCapacity(0);

		while(swimmingPool.getPool().getClientsNum() > 0){
			System.out.println("Jean still sleeps because there are "+swimmingPool.getPool().getClientsNum()+" clients in the pool");
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	//Method called once Jean finishes cleaning. It allows clients to go into the pool.
	public synchronized void finishCleaning(){
    	this.setCapacity(nbPlacesInitial);
    	notifyAll();
	}


}
