package com.company;


import static java.lang.Thread.sleep;

public class ChangingRoom extends SwimmingPoolPlace {
	public ChangingRoom(SwimmingPool swimmingPool, int nbPlaces, String placeName) {
		super(swimmingPool, nbPlaces, placeName);
	}


	@Override
	public void doAction(Client client) {
		System.out.println("Client " + client.getClientName() + " started changing clothes");

		//change clothes
		try {
			sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		client.setReadyToSwim(!client.isReadyToSwim());


		this.removeClient(client);
		System.out.println("Client " + client.getClientName() + " finished changing clothes");

		if (!client.isReadyToSwim()) {
			//stop the client (go out of the swimming pool)
			client.setRunning(false);
		} else {
			//redirect the client to the pool (60%) OR the swimfin store (40%)
			if (Math.random() >= 0.4)
				swimmingPool.getPool().addClient(client);
			else
				swimmingPool.getSwimfinStore().addClient(client);
		}
	}
}
