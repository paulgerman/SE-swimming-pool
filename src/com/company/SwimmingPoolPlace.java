package com.company;

public abstract class SwimmingPoolPlace extends ClientsHandler implements SwimmingPoolPlaceInterface {
	protected SwimmingPool swimmingPool;
	private String placeName;

	public SwimmingPoolPlace(SwimmingPool swimmingPool, int capacity, String placeName) {
		super(capacity);
		this.swimmingPool = swimmingPool;
		this.placeName = placeName;
	}

	@Override
	public synchronized void addClient(Client client) {
		//set the client place to the current place
		client.setCurrentPlace(this);
		super.addClient(client);
	}

	public String getPlaceName() {
		return placeName;
	}
}
