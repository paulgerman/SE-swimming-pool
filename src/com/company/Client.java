package com.company;

/**
 * Created by martinpotel on 16/11/2017.
 */
public class Client extends Thread {


	private String clientName;
    private SwimmingPool swimmingPool;


	private SwimmingPoolPlace currentPlace;

	private boolean running;
	private boolean readyToSwim;
	private boolean hasSwimfins;


    public Client (SwimmingPool swimmingPool, String clientName) {
    	this.swimmingPool = swimmingPool;
	    this.clientName = clientName;
    	readyToSwim = false;
    	running = true;
    }

	@Override
	public void run() {
		super.run();
		System.out.println("Client "+ clientName +": is running");

		swimmingPool.addClient(this);

		while(running)
			currentPlace.doAction(this);
		System.out.println("Client " + getClientName() + " left the building");
	}


	public String getClientName() {
		return clientName;
	}


	public void setCurrentPlace(SwimmingPoolPlace currentPlace) {
    	if(this.currentPlace != null)
			System.out.println("Client "+ clientName + ": Trying to change room from " + this.currentPlace.getPlaceName() + " to " + currentPlace.getPlaceName());
    	else
		    System.out.println("Client "+ clientName + ": Trying to change room to " + currentPlace.getPlaceName());

		this.currentPlace = currentPlace;
	}

	public boolean isReadyToSwim() {
		return readyToSwim;
	}

	public void setReadyToSwim(boolean readyToSwim) {
		this.readyToSwim = readyToSwim;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public boolean hasSwimfins() {
		return hasSwimfins;
	}

	public void setHasSwimfins(boolean hasSwimfins) {
		this.hasSwimfins = hasSwimfins;
	}

	public SwimmingPoolPlace getCurrentPlace() {
		return currentPlace;
	}
}
