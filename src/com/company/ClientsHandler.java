package com.company;

import java.util.ArrayList;

public abstract class ClientsHandler {
	private ArrayList<Client> clients;
	private int capacity;
	private int clientsWaiting;

	public ClientsHandler(int capacity) {
		this.capacity = capacity;
		clients = new ArrayList<>();
	}

	//add client to the clients list if there is enough capacity, otherwise go to sleep
	public synchronized void addClient(Client client) {
		while (clients.size() >= capacity) {
			//System.out.println("Client " + client.getClientName() + ": went to sleep, no more places left");
			clientsWaiting++;
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			clientsWaiting--;
		}
		clients.add(client);
		//System.out.println("Client " + client.getClientName() + ": changed room to "+ client.getCurrentPlace().getPlaceName());
	}

	//remove client from the clients list and notify the clients
	public synchronized void removeClient(Client client) {
		clients.remove(client);
		notifyAll();
	}

	public int getClientsNum() {
		return clients.size();
	}

	public int getClientsWaitingNum() {
		return clientsWaiting;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
}
