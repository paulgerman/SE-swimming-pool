package com.company;

import java.util.ArrayList;

/**
 * Created by martinpotel on 16/11/2017.
 */
public class SwimfinStore extends SwimmingPoolPlace {

	private int nbSwimfins;

	public SwimfinStore(SwimmingPool swimmingPool, int nbPlaces, int nbSwimfin, String name) {
		super(swimmingPool, nbPlaces, name);
		this.nbSwimfins = nbSwimfin;
	}

	@Override
	public synchronized void doAction(Client client) {

		if (!client.hasSwimfins()) {
			System.out.println("Client " + client.getClientName() + " started getting swimfins");
			//wait if not enough swimfins are available
			while (nbSwimfins < 2) {
				try {
					System.out.println("Client " + client.getClientName() + " is waiting for swimfins");
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			//give swimfins to the client
			nbSwimfins -= 2;
			client.setHasSwimfins(true);
			System.out.println("Client " + client.getClientName() + " finished getting swimfins");
		} else {
			//return the swimfins and notify the clients about availability
			nbSwimfins += 2;
			client.setHasSwimfins(false);
			System.out.println("Client " + client.getClientName() + " gave up his swimfins");
			notifyAll();
		}

		this.removeClient(client);

		//move client to next room
		if (client.hasSwimfins())
			swimmingPool.getPool().addClient(client);
		else
			swimmingPool.getChangingRoom().addClient(client);
	}
}
